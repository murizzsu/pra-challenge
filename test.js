const {tambah, kurang, bagi, kali} = require("./hitung")
const functionName = process.argv[2]
let passed = false

switch (functionName) {
    case "tambah":
        passed = assertEqual(2.4, tambah(2, 0.4));
    break;
    case "kurang":
        passed = assertEqual(1.6, kurang(2, 0.4));
    break;
    case "bagi":
        passed = assertEqual(5, bagi(2, 0.4));
    break;
    case "kali":
        passed = assertEqual(0.8, kali(2, 0.4));
    break;
    default:
        console.error(`fungsi ${functionName}() tidak dikenal`);
        passed = false;
        break;
    }
    
if (!passed) {
    process.exit(1);
}

function assertEqual(expected, reality) {
    if (reality !== expected) {
        console.error(`FAIL: fungsi ${functionName}() tidak sesuai expetasi: ${expected}, hasil: ${reality}`);
        return false;
    } else {
        console.log(`PASSED: ${functionName}()`)
        return true;
    }
}