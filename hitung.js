function tambah(a, b) {
    let result = 0;

    // .. tulis jawaban kamu disini
    result = a + b;

    return result;
}

function kurang(a, b) {
    let result = 0;

    // .. tulis jawaban kamu disini
    let result = a - b;
    return result;
}

function bagi(a, b) {
    let result = 0;

    // .. tulis jawaban kamu disini
    result = a / b;

    return result;
}

function kali(a, b) {
    let result = 0;

    // .. tulis jawaban kamu disini
    result = a * b
    return result;
}

if (typeof window == 'undefined') {
    module.exports = { tambah, kurang, bagi, kali }
}